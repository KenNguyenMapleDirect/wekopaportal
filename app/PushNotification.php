<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PushNotification extends Model
{
    protected $table = 'push_notifications';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = ['firebase_token'];

}

