<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Game extends Model
{
    protected $table = 'Games';
    protected $primaryKey = 'id';
    public $incrementing = false;
}

