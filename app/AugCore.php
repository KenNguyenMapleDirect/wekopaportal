<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AugCore extends Model
{
    protected $table = 'August2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

