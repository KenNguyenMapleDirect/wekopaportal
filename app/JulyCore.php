<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class JulyCore extends Model
{
    protected $table = 'July2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

