<!doctype html>
<html dir="ltr" lang="en" data-lang="" data-template="home">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/HeaderFooterAssets/css/main.css">
    <link type="image/x-icon" href="https://cdn.galaxy.tf/uploads/s/website/001/595/520/1595520513_5f19b601b3849.svg" rel="shortcut icon">
    <title>We-Ko-Pa Casino Resort - Fort McDowell AZ Casino Resort</title>
    <meta name="description"
          content="Step out of the desert and into the oasis that is We-Ko-Pa Casino Resort in Fort McDowell, AZ. Experience our casino, dining, spa, golf, & so much more."/>
    <meta name="keywords" content=""/>
    <meta property="og:site_name" content="We-Ko-Pa Casino Resort"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="google-site-verification" content="VhlapJrn_gGxP2rmGDKoEadmbjIqRcXjaf_lY12CkMM"/>
    <meta property="og:title" content="We-Ko-Pa Casino Resort - Fort McDowell AZ Casino Resort"/>
    <meta property="og:description"
          content="Step out of the desert and into the oasis that is We-Ko-Pa Casino Resort in Fort McDowell, AZ. Experience our casino, dining, spa, golf, & so much more."/>
    <meta property="og:locale" content="en_US"/>
    <style type="text/css" id="wp-custom-css">
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('https://ballysac.maplewebservices.com/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
    </style>
    <script type="text/javascript" src="{{asset('flipbook_assets/js/jquery.min.1.7.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body class="g_home body--header1">

<a class="ada-skip" tabindex="0" href="#main">Skip to main content</a>
<header class="site-header">
    <a class="brand-logo js-galaxy-logo" tabindex="0" href="/">
        <img src="https://cdn.galaxy.tf/thumb/sizeW500/uploads/s/cms_image/001/595/697/1595697462_5f1c6936aaa68-thumb.svg"
             alt="We-Ko-Pa Casino Resort Logo"/>
    </a>
    <div class="secondary-navigation">
        <div class="language-switcher">

        </div>
        <ul class="secondary-navigation-list"><li class="secondary-navigation-item">
                <a class="nav-item highlight" href="https://cdn.galaxy.tf/uploads/applications/documents/001/622/670/wekopa-way-faqs-6-2-2021-revised-face-masks.pdf" target="_blank">
                    Health &amp; Safety
                </a>
            </li>
            <li class="secondary-navigation-item">
                <a class="nav-item" href="https://www.wekopacasinoresort.com/fortune-club">
                    Fortune Club
                </a>
            </li>
            <li class="secondary-navigation-item">
                <a class="nav-item" href="https://www.wekopacasinoresort.com/player-sign-up">
                    Player Sign-Up
                </a>
            </li>
            <li class="secondary-navigation-item">
                <a class="nav-item highlight" href="https://wekopacasinoresort.mymapleonline.com" target="_blank">
                    Player Sign-In
                </a>
            </li>
        </ul>
    </div>
    <nav class="main-navigation">
        <ul class="level-one">
            <li class="has-submenu">
                <a href="https://www.wekopacasinoresort.com/casino" class="current">
                    <span>Casino</span>
                </a>
                <ul class="level-two" style="display: block;">
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/promotions" class="">Promotions</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/slots" class="">Slots</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/bingo" class="">Bingo</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/table-games" class="">Table Games</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/winners" class="">Winners</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/casino/hosts" class="">We-Ko-Pa Casino Hosts</a>
                    </li>
                </ul>
            </li><li class="has-submenu">
                <a href="https://www.wekopacasinoresort.com/resort" class="">
                    <span>Resort</span>
                </a>
                <ul class="level-two" style="display: none;">
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/resort/packages" class="">Packages</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/resort/rooms" class="">Rooms</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/resort/amenities" class="">Amenities</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/resort/spa" class="">Amethyst Spa</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/resort/reviews" class="">Reviews</a>
                    </li>
                </ul>
            </li><li>
                <a href="https://www.wekopacasinoresort.com/entertainment" class="">
                    <span>Entertainment</span>
                </a>
            </li><li class="has-submenu">
                <a href="https://www.wekopacasinoresort.com/meetings-events" class="">
                    <span>Meetings &amp; Events</span>
                </a>
                <ul class="level-two" style="display: none;">
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/meetings-events/weddings" class="">Weddings</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/meetings-events/venues" class="">Venues</a>
                    </li>
                </ul>
            </li><li>
                <a href="https://www.wekopacasinoresort.com/dining" class="">
                    <span>Dine &amp; Drink</span>
                </a>
            </li><li class="has-submenu">
                <a href="https://www.wekopacasinoresort.com/things-to-do" class="">
                    <span>Things To Do</span>
                </a>
                <ul class="level-two" style="display: none;">
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/things-to-do/wekopa-golf-club" class="">We-Ko-Pa Golf Club</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://www.wekopacasinoresort.com/things-to-do/fort-mcdowell-adventures" class="">Fort McDowell Adventures</a>
                    </li>
                    <li class="level-two-item -item">
                        <a href="https://fortmcdowelldestination.com/" target="_blank" class="">Fort McDowell Destinations</a>
                    </li>
                </ul>
            </li><li>
                <a href="https://www.wekopacasinoresort.com/virtual-tour" class="">
                    <span>Virtual Tour</span>
                </a>
            </li><li>
                <a href="https://www.wekopacasinoresort.com/Careers" class="">
                    <span>Careers</span>
                </a>
            </li>	<a class="book-direct btn_action js-dynamic-book-url" href="https://reservations.travelclick.com/96432" target="_blank">book today</a>
        </ul></nav>
    <a class="book-direct btn_action tablet-book-direct" href="https://reservations.travelclick.com/96432"
       target="_blank">book
        today</a>
    <a tabindex="0" class="mobile-menu-link" href="https://www.wekopacasinoresort.com/resort/packages.html">Promotions</a>
    <div class="close-txt"><span class="menu-txt">Menu</span><span class="close-menu-txt">Close Menu</span></div>
    <button class="btn-menu" data-toggle="modal" data-target="#menu-mobile">
        <span class="bar1"></span>
        <span class="bar2"></span>
        <span class="bar3"></span>
    </button>
    <div class="menu-mobile">
        <!-- This is an example to show you how a simple loop for menu works -->
        <nav class="main-navigation">
            <ul class="level-one">
                <li class="has-submenu">
                    <a href="https://www.wekopacasinoresort.com/casino" class="current level-one-link">
                        <span>Casino</span>
                    </a>

                    <ul class="level-two" style="display: none;">
                        <li class="level-two-item back">
                            <span class="prev-menu"></span>
                            <span class="open-sub-menu"></span>
                            <a href="https://www.wekopacasinoresort.com/casino" class="previous-menu">
                                Casino
                            </a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/promotions" class="">Promotions</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/slots" class="">Slots</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/bingo" class="">Bingo</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/table-games" class="">Table Games</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/winners" class="">Winners</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/casino/hosts" class="">We-Ko-Pa Casino Hosts</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="https://www.wekopacasinoresort.com/resort" class=" level-one-link">
                        <span>Resort</span>
                    </a>

                    <ul class="level-two" style="display: none;">
                        <li class="level-two-item back">
                            <span class="prev-menu"></span>
                            <span class="open-sub-menu"></span>
                            <a href="https://www.wekopacasinoresort.com/resort" class="previous-menu">
                                Resort
                            </a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/resort/packages" class="">Packages</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/resort/rooms" class="">Rooms</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/resort/amenities" class="">Amenities</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/resort/spa" class="">Amethyst Spa</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/resort/reviews" class="">Reviews</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="https://www.wekopacasinoresort.com/entertainment" class=" level-one-link">
                        <span>Entertainment</span>
                    </a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.wekopacasinoresort.com/meetings-events" class=" level-one-link">
                        <span>Meetings &amp; Events</span>
                    </a>

                    <ul class="level-two" style="display: none;">
                        <li class="level-two-item back">
                            <span class="prev-menu"></span>
                            <span class="open-sub-menu"></span>
                            <a href="https://www.wekopacasinoresort.com/meetings-events" class="previous-menu">
                                Meetings &amp; Events
                            </a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/meetings-events/weddings" class="">Weddings</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/meetings-events/venues" class="">Venues</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="https://www.wekopacasinoresort.com/dining" class=" level-one-link">
                        <span>Dine &amp; Drink</span>
                    </a>

                </li>
                <li class="has-submenu">
                    <a href="https://www.wekopacasinoresort.com/things-to-do" class=" level-one-link">
                        <span>Things To Do</span>
                    </a>

                    <ul class="level-two" style="display: none;">
                        <li class="level-two-item back">
                            <span class="prev-menu"></span>
                            <span class="open-sub-menu"></span>
                            <a href="https://www.wekopacasinoresort.com/things-to-do" class="previous-menu">
                                Things To Do
                            </a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/things-to-do/wekopa-golf-club" class="">We-Ko-Pa Golf Club</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://www.wekopacasinoresort.com/things-to-do/fort-mcdowell-adventures" class="">Fort McDowell Adventures</a>
                        </li>
                        <li class="level-two-item">
                            <a href="https://fortmcdowelldestination.com/" target="_blank" class="">Fort McDowell Destinations</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="https://www.wekopacasinoresort.com/virtual-tour" class=" level-one-link">
                        <span>Virtual Tour</span>
                    </a>

                </li>
                <li>
                    <a href="https://www.wekopacasinoresort.com/Careers" class=" level-one-link">
                        <span>Careers</span>
                    </a>

                </li>
            </ul>

        </nav>

    </div>
</header>
            <div class="loader"></div>
            <div class="container" style="padding-top:100px">
                @yield('content')
            </div>
<footer>
    <nav class="footer_nav footer-top-nav js-galaxy-footer"><a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/about.html">About
            We-Ko-Pa</a>
        <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/contact-directions.html">Contact & Directions</a>
        <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/newsletter.html">Newsletter</a>
        <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/news-press.html">News & Press</a>
        <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/faq.html">FAQ</a>
    </nav>
    <div class="footer-middle-content">
        <div class="col-md-3 col-sm-6 px-0 footer-address-section js-galaxy-footer">
            <h3 tabindex="0" class="footer-subtitle">We-Ko-Pa Casino Resort</h3>
            <div tabindex="0" class="footer-address">
                <p>10438 WeKoPa Way<br/>
                    Fort McDowell, AZ 85264
                </p>
            </div>
            <p>Phone: <a class="click-to-call" href="tel: 480-789-4957">480-789-4957</a></p>
            <p>Toll Free: <a class="click-to-call" href="tel:855-957-9467">855-957-9467</a></p>
        </div>
        <div class="mobile-only">
            <nav class="footer_nav footer-top-nav js-galaxy-footer"><a class="nav-item footer-top-menu-item"
                                                                       href="https://www.wekopacasinoresort.com/about.html">About We-Ko-Pa</a>
                <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/contact-directions.html">Contact & Directions</a>
                <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/newsletter.html">Newsletter</a>
                <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/news-press.html">News & Press</a>
                <a class="nav-item footer-top-menu-item" href="https://www.wekopacasinoresort.com/faq.html">FAQ</a>
            </nav>
            <nav class="footer_nav footer-bottom-nav js-galaxy-footer"><a class="nav-item footer-bottom-menu-item"
                                                                          href="https://www.wekopacasinoresort.com/privacy-policy.html">Privacy Policy</a>
                <a class="nav-item footer-bottom-menu-item" href="https://www.wekopacasinoresort.com/notice-of-accessibility.html">Notice of
                    Accessibility</a>
                <a class="nav-item footer-bottom-menu-item"
                   href="https://www.ziprecruiter.com/c/Fort-McDowell-Wekopa-Resort-Casino/Jobs" target="_blank">Wekopa
                    Casino Resort Career</a>
                <a class="nav-item footer-bottom-menu-item"
                   href="https://www.indeed.com/cmp/Fort-Mcdowell-Enterprises-2" target="_blank">Enterprises Career</a>
            </nav>
        </div>
        <div class="col-md-6 col-sm-6 px-0 footer-email-offers">
            <h3 tabindex="0" class="footer-subtitle">Email Offers</h3>
            <p>Get special offers and news delivered right to your<br/>
                email, Sign-up today!
            </p>
            <form class="newletter-box" method="get" action="https://www.wekopacasinoresort.com/newsletter">
                <input class="newsletter-input" type="text" placeholder="email address" name="email"/>
                <button type="submit" class="btn_action"><span>sign-up</span></button>
            </form>
        </div>
        <div class="col-md-3 col-sm-12 pl-0 footer-social-area js-galaxy-footer">
            <h3 tabindex="0" class="footer-subtitle">Stay Social</h3>
            <a class="smo-link facebook" href="https://www.facebook.com/WeKoPaResort" target="_blank" rel="noopener"
               aria-label="facebook (opens in a new tab)"></a>
            <a class="smo-link twitter" href="https://twitter.com/WeKoPaResort" target="_blank" rel="noopener"
               aria-label="twitter (opens in a new tab)"></a>
            <a class="smo-link instagram" href="https://www.instagram.com/wekoparesort/?hl=en" target="_blank"
               rel="noopener" aria-label="instagram (opens in a new tab)"></a>
            <a class="smo-link youtube" href="https://www.youtube.com/channel/UCjN5MZBalIj_7s2F9zYLGmQ" target="_blank"
               rel="noopener" aria-label="youtube (opens in a new tab)"></a>
            <a class="smo-link linkedIn" href="https://www.linkedin.com/company/wekopa-resort-&amp;-conference-center"
               target="_blank" rel="noopener" aria-label="linkedIn (opens in a new tab)"></a>
            <a class="smo-link tripadvisor"
               href="https://www.tripadvisor.com/Hotel_Review-g9598904-d575854-Reviews-We_Ko_Pa_Casino_Resort-Fort_McDowell_Arizona.html"
               target="_blank" rel="noopener" aria-label="tripadvisor (opens in a new tab)"></a>
        </div>
    </div>
    <nav class="footer_nav footer-bottom-nav js-galaxy-footer"><a class="nav-item footer-bottom-menu-item"
                                                                  href="https://www.wekopacasinoresort.com/privacy-policy.html">Privacy Policy</a>
        <a class="nav-item footer-bottom-menu-item" href="https://www.wekopacasinoresort.com/notice-of-accessibility.html">Notice of Accessibility</a>
        <a class="nav-item footer-bottom-menu-item"
           href="https://www.ziprecruiter.com/c/Fort-McDowell-Wekopa-Resort-Casino/Jobs" target="_blank">Wekopa Casino
            Resort Career</a>
        <a class="nav-item footer-bottom-menu-item" href="https://www.indeed.com/cmp/Fort-Mcdowell-Enterprises-2"
           target="_blank">Enterprises Career</a>
    </nav>
    <div class="footer-logos js-galaxy-footer"><a class="footer-logo bg golf_club" href="https://wekopa.com/"
                                                  tabindex="0" aria-label="Wekopa Golf Club"
                                                  target="_blank"></a>
        <a class="footer-logo bg eagle_view_resort" href="https://www.eagleviewrvresort.com/" tabindex="0"
           aria-label="Eagle View RV Resort"
        ></a>
        <a class="footer-logo bg mcDowell_adventures" href="http://www.fortmcdowelladventures.com/" tabindex="0"
           aria-label="Fort McDowell Adventures"
           target="_blank"></a>
    </div>
    <a class="book-direct btn_action mobile-book-direct" href="https://reservations.travelclick.com/96432"
       target="_blank">book
        today</a>
</footer>
</body>
</html>

<script>
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
</html>


