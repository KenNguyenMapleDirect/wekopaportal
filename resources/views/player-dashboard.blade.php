@extends('layouts.app')

@section('content')
    <div class="row justify-content-center" style="padding-top: 20px;background: white">
        <div class="col-md-9">

        @if($data->flgSM || $data->flgPC)
            <!-- SM here -->
                <div class="row">
                    @if($data->augCoreSM)
                        <div class="col-md-12">
                            <meta name="viewport" content="width = 1050, user-scalable = yes"/>
                            <script type="text/javascript"
                                    src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                            <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                            <script type="text/javascript"
                                    src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                            <script type="text/javascript"
                                    src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                            <style>
                                table {
                                    border-collapse: inherit !important;
                                }

                                .lb-details {
                                    display: none;
                                }

                                @font-face {
                                    font-family: myFirstFont;
                                    src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                }

                                .firstname {
                                    font-family: myFirstFont;
                                    font-weight: 700;
                                    font-size: 16pt;
                                }

                                @font-face {
                                    font-family: totalfb;
                                    src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                }

                                .dollar-amount {
                                    font-family: 'totalfb';
                                    font-weight: 700;
                                    font-size: 16pt;
                                }

                                /* .dollar-amount62 {
                                  font-family: 'Steelfish', sans-serif;
                                  font-weight: 700;
                                  font-size: 16pt;
                                } */

                                #img-magnifier-container {
                                    display: none;
                                    background: rgba(0, 0, 0, 0.8);
                                    border: 5px solid rgb(255, 255, 255);
                                    border-radius: 20px;
                                    box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                    0 0 7px 7px rgba(0, 0, 0, 0.25),
                                    inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                    cursor: none;
                                    position: absolute;
                                    pointer-events: none;
                                    width: 400px;
                                    height: 200px;
                                    overflow: hidden;
                                    z-index: 999;
                                }

                                .glass {
                                    position: absolute;
                                    background-repeat: no-repeat;
                                    background-size: auto;
                                    cursor: none;
                                    z-index: 1000;
                                }

                                #toggle-zoom {
                                    background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                    background-size: 40px;
                                    display: block;
                                    width: 40px;
                                    display: none;
                                    height: 40px;
                                }

                                #printer {
                                    float: right;
                                    display: block;
                                    width: 40px;
                                    height: 40px;
                                    margin-right: 20px;
                                    display: none;
                                }

                                #toggle-zoom.toggle-on {
                                    background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                }

                                @media (hover: none) {
                                    .tool-zoom {
                                        display: none;
                                    }

                                    #printer {
                                        display: none;
                                    }
                                }
                            </style>
                            <div class="flipbook-viewport">
                                <div class="container">
                                    <div>
                                        <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}"" target="blank"><img
                                                id="printer"
                                                src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                    </div>
                                    <div class="tool-zoom">
                                        <a id="toggle-zoom" onclick="toggleZoom()"></a>
                                    </div>

                                    <div class="arrows">
                                        <div class="arrow-prev">
                                            <a id="prev"><img class="previous" width="20"
                                                              src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                              alt=""/></a>
                                        </div>
                                        <center><h4 style="color: red;margin-right: 35px;"> August Core Self
                                                Mailer</h4>
                                        </center>
                                        <div class="flipbook">
                                            <!-- Below is where you change the images for the flipbook -->
                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult1}}"
                                               data-odd="1"
                                               id="page-1"
                                               data-lightbox="big" class="page"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult1}}')"></a>

                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult2}}"
                                               data-even="2"
                                               id="page-2" data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult2}}')"></a>

                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult3}}"
                                               data-odd="3"
                                               id="page-3"
                                               data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult3}}')"></a>

                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult4}}"
                                               data-even="4"
                                               id="page-4" data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult4}}')"></a>

                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult5}}"
                                               data-odd="5"
                                               id="page-5"
                                               data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult5}}')"></a>

                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/lo_res/{{$data->augCoreSMResult6}}"
                                               data-even="6"
                                               id="page-6" data-lightbox="big" class="single"
                                               style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/lo_res/{{$data->augCoreSMResult6}}')"></a>
                                        </div>
                                        <div class="arrow-next">
                                            <a id="next"><img class="next" width="20"
                                                              src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                              alt=""/></a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Below are the thumbnails to the flipbook -->
                            <div class="flipbook-slider-thumb">
                                <div class="drag" style="margin-right: 35px;">
                                    <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                    <img onclick="onPageClick(1)" class="thumb-img left-img"
                                         src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult1}}"
                                         alt=""/>

                                    <div class="space">
                                        <img onclick="onPageClick(2)" class="thumb-img"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult2}}"
                                             alt=""/>
                                        <img onclick="onPageClick(3)" class="thumb-img"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult3}}"
                                             alt=""/>
                                    </div>

                                    <div class="space">
                                        <img onclick="onPageClick(4)" class="thumb-img"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult4}}"
                                             alt=""/>
                                        <img onclick="onPageClick(5)" class="thumb-img"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCoreSMResult5}}"
                                             alt=""/>
                                    </div>

                                    <div class="space">
                                        <img onclick="onPageClick(6)" class="thumb-img active"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/lo_res/{{$data->augCoreSMResult6}}"
                                             alt=""/>
                                    </div>

                                <!-- <img id="next-arrow" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                </div>

                                <ul class="flipbook-slick-dots" role="tablist">
                                    <li onclick="onPageClick(1)" class="dot">
                                        <a type="button" style="color: #7f7f7f">1</a>
                                    </li>
                                    <li onclick="onPageClick(2)" class="dot">
                                        <a type="button" style="color: #7f7f7f">2</a>
                                    </li>
                                    <li onclick="onPageClick(3)" class="dot">
                                        <a type="button" style="color: #7f7f7f">3</a>
                                    </li>
                                    <li onclick="onPageClick(4)" class="dot">
                                        <a type="button" style="color: #7f7f7f">4</a>
                                    </li>
                                    <li onclick="onPageClick(5)" class="dot">
                                        <a type="button" style="color: #7f7f7f">5</a>
                                    </li>
                                    <li onclick="onPageClick(6)" class="dot">
                                        <a type="button" style="color: #7f7f7f">6</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="img-magnifier-container">
                                <img id="zoomed-image-container" class="glass" src=""/>
                            </div>
                            <div id="log"></div>
                            <audio id="audio" style="display: none"
                                   src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                            <script type="text/javascript">
                                function scaleFlipBook() {
                                    var imageWidth = 541;
                                    var imageHeight = 850;

                                    var pageHeight = 550;
                                    var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                    $(".flipbook-viewport .container").css({
                                        width: 40 + pageWidth * 2 + 40 + "px",
                                    });

                                    $(".flipbook-viewport .flipbook").css({
                                        width: pageWidth * 2 + "px",
                                        height: pageHeight + "px",
                                    });
                                }

                                function doResize() {
                                    $("html").css({
                                        zoom: 1
                                    });
                                    var $viewport = $(".flipbook-viewport");
                                    var viewHeight = $viewport.height();
                                    var viewWidth = $viewport.width();

                                    var $el = $(".flipbook-viewport .container");
                                    var elHeight = $el.outerHeight();
                                    var elWidth = $el.outerWidth();

                                    var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                    //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                    if (scale < 1) {
                                        scale *= 0.95;
                                    } else {
                                        scale = 1;
                                    }
                                    $("html").css({
                                        zoom: scale
                                    });
                                }

                                function loadApp() {
                                    scaleFlipBook();
                                    var flipbook = $(".flipbook");

                                    // Check if the CSS was already loaded

                                    if (flipbook.width() == 0 || flipbook.height() == 0) {
                                        setTimeout(loadApp, 10);
                                        return;
                                    }

                                    $(".flipbook .double").scissor();

                                    // Create the flipbook

                                    $(".flipbook").turn({
                                        // Elevation

                                        elevation: 50,

                                        // Enable gradients

                                        gradients: true,

                                        // Auto center this flipbook

                                        autoCenter: true,
                                        when: {
                                            turning: function (event, page, view) {
                                                var audio = document.getElementById("audio");
                                                audio.play();
                                            },
                                            turned: function (e, page) {
                                                //console.log('Current view: ', $(this).turn('view'));
                                                var thumbs = document.getElementsByClassName("thumb-img");
                                                for (var i = 0; i < thumbs.length; i++) {
                                                    var element = thumbs[i];
                                                    if (element.className.indexOf("active") !== -1) {
                                                        $(element).removeClass("active");
                                                    }
                                                }

                                                $(
                                                    document.getElementsByClassName("thumb-img")[page - 1]
                                                ).addClass("active");

                                                var dots = document.getElementsByClassName("dot");
                                                for (var i = 0; i < dots.length; i++) {
                                                    var dot = dots[i];
                                                    if (dot.className.indexOf("dot-active") !== -1) {
                                                        $(dot).removeClass("dot-active");
                                                    }
                                                }
                                            },
                                        },
                                    });
                                    doResize();
                                }

                                $(window).resize(function () {
                                    doResize();
                                });
                                $(window).bind("keydown", function (e) {
                                    if (e.keyCode == 37) $(".flipbook").turn("previous");
                                    else if (e.keyCode == 39) $(".flipbook").turn("next");
                                });
                                $("#prev").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("previous");
                                });

                                $("#next").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("next");
                                });

                                $("#prev-arrow").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("previous");
                                });

                                $("#next-arrow").click(function (e) {
                                    e.preventDefault();
                                    $(".flipbook").turn("next");
                                });

                                function onPageClick(i) {
                                    $(".flipbook").turn("page", i);
                                }

                                // Load the HTML4 version if there's not CSS transform
                                yepnope({
                                    test: Modernizr.csstransforms,
                                    yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                    nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                    both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                                    complete: loadApp,
                                });

                                zoomToolEnabled = false;

                                function toggleZoom() {
                                    if (zoomToolEnabled) {
                                        $(".flipbook a").off("mousemove");
                                        $("#toggle-zoom").removeClass("toggle-on");
                                        $("#img-magnifier-container").hide();

                                        zoomToolEnabled = false;
                                    } else {
                                        $(".flipbook a").mousemove(function (event) {
                                            var magnifier = $("#img-magnifier-container");
                                            $("#img-magnifier-container").css(
                                                "left",
                                                event.pageX - magnifier.width() / 2
                                            );
                                            $("#img-magnifier-container").css(
                                                "top",
                                                event.pageY - magnifier.height() / 2
                                            );
                                            $("#img-magnifier-container").show();
                                            var hoveredImage = $(event.target).css("background-image");
                                            var bg = hoveredImage
                                                .replace("url(", "")
                                                .replace(")", "")
                                                .replace(/\"/gi, "");
                                            // Find relative position of cursor in image.
                                            var targetPage = $(event.target);
                                            var targetLeft = 400 / 2; // Width of glass container/2
                                            var targetTop = 200 / 2; // Height of glass container/2

                                            var zoomedImageContainer = document.getElementById(
                                                "zoomed-image-container"
                                            );
                                            var zoomedImageWidth = zoomedImageContainer.width;
                                            var zoomedImageHeight = zoomedImageContainer.height;

                                            var imgXPercent =
                                                (event.pageX - $(event.target).offset().left) /
                                                targetPage.width();
                                            targetLeft -= zoomedImageWidth * imgXPercent;
                                            var imgYPercent =
                                                (event.pageY - $(event.target).offset().top) /
                                                targetPage.height();
                                            targetTop -= zoomedImageHeight * imgYPercent;

                                            $("#img-magnifier-container .glass").attr("src", bg);
                                            $("#img-magnifier-container .glass").css(
                                                "top",
                                                "" + targetTop + "px"
                                            );
                                            $("#img-magnifier-container .glass").css(
                                                "left",
                                                "" + targetLeft + "px"
                                            );
                                        });

                                        $("#toggle-zoom").addClass("toggle-on");
                                        zoomToolEnabled = true;
                                    }
                                }
                            </script>
                        </div>
                    @endif
                    @if($data->sepCoreSM)
                            <div class="col-md-12">
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/lightbox-second-scroll-to-top.min.js')}}"></script>
                                <style>
                                    table {
                                        border-collapse: inherit !important;
                                    }

                                    .lb-details {
                                        display: none;
                                    }

                                    @font-face {
                                        font-family: myFirstFont;
                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                    }

                                    .firstname {
                                        font-family: myFirstFont;
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    @font-face {
                                        font-family: totalfb;
                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                    }

                                    .dollar-amount {
                                        font-family: 'totalfb';
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    /* .dollar-amount62 {
                                      font-family: 'Steelfish', sans-serif;
                                      font-weight: 700;
                                      font-size: 16pt;
                                    } */

                                    #img-magnifier-container-second {
                                        display: none;
                                        background: rgba(0, 0, 0, 0.8);
                                        border: 5px solid rgb(255, 255, 255);
                                        border-radius: 20px;
                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                        cursor: none;
                                        position: absolute;
                                        pointer-events: none;
                                        width: 400px;
                                        height: 200px;
                                        overflow: hidden;
                                        z-index: 999;
                                    }

                                    .glass {
                                        position: absolute;
                                        background-repeat: no-repeat;
                                        background-size: auto;
                                        cursor: none;
                                        z-index: 1000;
                                    }

                                    #toggle-zoom-second {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                        background-size: 40px;
                                        display: block;
                                        width: 40px;
                                        display: none;
                                        height: 40px;
                                    }

                                    #printer {
                                        float: right;
                                        display: block;
                                        width: 40px;
                                        height: 40px;
                                        margin-right: 20px;
                                        display: none;
                                    }

                                    #toggle-zoom-second.toggle-on {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                    }

                                    @media (hover: none) {
                                        .tool-zoom {
                                            display: none;
                                        }

                                        #printer {
                                            display: none;
                                        }
                                    }
                                </style>
                                <div class="flipbook-viewport-second">
                                    <div class="container">
                                        <div>
                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                               target="blank"><img
                                                    id="printer"
                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                        </div>
                                        <div class="tool-zoom">
                                            <a id="toggle-zoom-second" onclick="toggleZoom()"></a>
                                        </div>

                                        <div class="arrows">
                                            <div class="arrow-prev">
                                                <a id="prev-second"><img class="previous" width="20"
                                                                         src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                         alt=""/></a>
                                            </div>
                                            <center><h4 style="color: red;">September Core Self Mailer
                                                    01</h4>
                                            </center>
                                            <div class="flipbook-second">

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult1}}"
                                                   data-odd="1"
                                                   id="page-1"
                                                   data-lightbox-second="big" class="page"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult1}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult2}}"
                                                   data-even="2"
                                                   id="page-2" data-lightbox-second="big"
                                                   class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult2}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult3}}"
                                                   data-odd="3"
                                                   id="page-3"
                                                   data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult3}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult4}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox-second="big"
                                                   class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/lo_res/{{$data->sepCoreSMResult4}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult5}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox-second="big"
                                                   class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/lo_res/{{$data->sepCoreSMResult5}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult6}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox-second="big"
                                                   class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/lo_res/{{$data->sepCoreSMResult6}}')"></a>
                                            </div>
                                            <div class="arrow-next">
                                                <a id="next-second"><img class="next" width="20"
                                                                         src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                         alt=""/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flipbook-slider-thumb-second">
                                    <div class="drag-second">
                                        <!-- <img id="prev-arrow-second" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                        <img onclick="onPageClickSecond(1)"
                                             class="thumb-img left-img"
                                             src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult1}}"
                                             alt=""/>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(2)" class="thumb-img"
                                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult2}}"
                                                 alt=""/>
                                            <img onclick="onPageClickSecond(3)" class="thumb-img"
                                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult3}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(4)" class="thumb-img"
                                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult4}}"
                                                 alt=""/>
                                            <img onclick="onPageClickSecond(5)" class="thumb-img"
                                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCoreSMResult5}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(6)"
                                                 class="thumb-img active"
                                                 src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/lo_res/{{$data->sepCoreSMResult6}}"
                                                 alt=""/>
                                        </div>


                                    <!-- <img id="next-arrow-second" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                    </div>

                                    <ul class="flipbook-slick-dots-second" role="tablist">
                                        <li onclick="onPageClickSecond(1)" class="dot">
                                            <a type="button" style="color: #7f7f7f">1</a>
                                        </li>
                                        <li onclick="onPageClickSecond(2)" class="dot">
                                            <a type="button" style="color: #7f7f7f">2</a>
                                        </li>
                                        <li onclick="onPageClickSecond(3)" class="dot">
                                            <a type="button" style="color: #7f7f7f">3</a>
                                        </li>
                                        <li onclick="onPageClickSecond(4)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                        <li onclick="onPageClickSecond(5)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                        <li onclick="onPageClickSecond(6)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="img-magnifier-container-second">
                                    <img id="zoomed-image-container" class="glass" src=""/>
                                </div>
                                <div id="log"></div>
                                <audio id="audio" style="display: none"
                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                <script type="text/javascript">
                                    function scaleFlipBookSecond() {
                                        var imageWidth = 541;
                                        var imageHeight = 850;
                                        var pageHeight = 550;
                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                        $(".flipbook-viewport-second .container").css({
                                            width: 40 + pageWidth * 2 + 40 + "px",
                                        });

                                        $(".flipbook-viewport-second .flipbook-second").css({
                                            width: pageWidth * 2 + "px",
                                            height: pageHeight + "px",
                                        });

                                        $(".flipbook-viewport-second .flipbook-second").css('margin-bottom', 20);
                                    }

                                    function doResizeSecond() {
                                        $("html").css({
                                            zoom: 1
                                        });
                                        var $viewportSecond = $(".flipbook-viewport-second");
                                        var viewHeight = $viewportSecond.height();
                                        var viewWidth = $viewportSecond.width();

                                        var $el = $(".flipbook-viewport-second .container");
                                        var elHeight = $el.outerHeight();
                                        var elWidth = $el.outerWidth();

                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                        if (scale < 1) {
                                            scale *= 0.95;
                                        } else {
                                            scale = 1;
                                        }
                                        // $("html").css({
                                        //     zoom: scale
                                        // });
                                    }

                                    function loadAppSecond() {
                                        scaleFlipBookSecond();
                                        var flipbookSecond = $(".flipbook-second");

                                        // Check if the CSS was already loaded

                                        if (flipbookSecond.width() == 0 || flipbookSecond.height() == 0) {
                                            setTimeout(loadAppSecond, 10);
                                            return;
                                        }

                                        $(".flipbook-second .double").scissor();

                                        // Create the flipbook-second

                                        $(".flipbook-second").turn({
                                            // Elevation

                                            elevation: 50,

                                            // Enable gradients

                                            gradients: true,

                                            // Auto center this flipbook-second

                                            autoCenter: true,
                                            when: {
                                                turning: function (event, page, view) {
                                                    var audio = document.getElementById("audio");
                                                    audio.play();
                                                },
                                                turned: function (e, page) {
                                                    //console.log('Current view: ', $(this).turn('view'));
                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                    for (var i = 0; i < thumbs.length; i++) {
                                                        var element = thumbs[i];
                                                        if (element.className.indexOf("active") !== -1) {
                                                            $(element).removeClass("active");
                                                        }
                                                    }

                                                    $(
                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                    ).addClass("active");

                                                    var dots = document.getElementsByClassName("dot");
                                                    for (var i = 0; i < dots.length; i++) {
                                                        var dot = dots[i];
                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                            $(dot).removeClass("dot-active");
                                                        }
                                                    }
                                                },
                                            },
                                        });
                                        doResizeSecond();
                                    }

                                    $(window).resize(function () {
                                        doResizeSecond();
                                    });
                                    $(window).bind("keydown", function (e) {
                                        if (e.keyCode == 37) $(".flipbook-second").turn("previous");
                                        else if (e.keyCode == 39) $(".flipbook-second").turn("next");
                                    });
                                    $("#prev-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("previous");
                                    });

                                    $("#next-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("next");
                                    });

                                    $("#prev-arrow-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("previous");
                                    });

                                    $("#next-arrow-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("next");
                                    });

                                    function onPageClickSecond(i) {
                                        $(".flipbook-second").turn("page", i);
                                    }

                                    // Load the HTML4 version if there's not CSS transform
                                    yepnope({
                                        test: Modernizr.csstransforms,
                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page-second.css')}}"],
                                        complete: loadAppSecond,
                                    });

                                    zoomToolEnabled = false;

                                    function toggleZoom() {
                                        if (zoomToolEnabled) {
                                            $(".flipbook-second a").off("mousemove");
                                            $("#toggle-zoom-second").removeClass("toggle-on");
                                            $("#img-magnifier-container-second").hide();

                                            zoomToolEnabled = false;
                                        } else {
                                            $(".flipbook-second a").mousemove(function (event) {
                                                var magnifier = $("#img-magnifier-container-second");
                                                $("#img-magnifier-container-second").css(
                                                    "left",
                                                    event.pageX - magnifier.width() / 2
                                                );
                                                $("#img-magnifier-container-second").css(
                                                    "top",
                                                    event.pageY - magnifier.height() / 2
                                                );
                                                $("#img-magnifier-container-second").show();
                                                var hoveredImage = $(event.target).css("background-image");
                                                var bg = hoveredImage
                                                    .replace("url(", "")
                                                    .replace(")", "")
                                                    .replace(/\"/gi, "");
                                                // Find relative position of cursor in image.
                                                var targetPage = $(event.target);
                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                var targetTop = 200 / 2; // Height of glass container/2

                                                var zoomedImageContainer = document.getElementById(
                                                    "zoomed-image-container"
                                                );
                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                var imgXPercent =
                                                    (event.pageX - $(event.target).offset().left) /
                                                    targetPage.width();
                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                var imgYPercent =
                                                    (event.pageY - $(event.target).offset().top) /
                                                    targetPage.height();
                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                $("#img-magnifier-container-second .glass").attr("src", bg);
                                                $("#img-magnifier-container-second .glass").css(
                                                    "top",
                                                    "" + targetTop + "px"
                                                );
                                                $("#img-magnifier-container-second .glass").css(
                                                    "left",
                                                    "" + targetLeft + "px"
                                                );
                                            });

                                            $("#toggle-zoom-second").addClass("toggle-on");
                                            zoomToolEnabled = true;
                                        }
                                    }
                                </script>
                            </div>
                    @endif
                </div>

                <!-- PC here -->
                <div class="row">
                    @if($data->augDinnerPC)
                        <div class="col-md-3">
                            <h4 style="color: red;text-align: center;">August Dinner Mailer</h4>
                            <body>
                            <div class="modal-wrap">
                                <div class="slider-big">
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult1}}"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult1}}"
                                            style="width: 20%;margin: 0 auto;" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult2}}"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult2}}"
                                            style="width: 100%" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult3}}"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult3}}"
                                            style="width: 20%;margin: 0 auto;" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult4}}"
                                       data-lightbox-fifth="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augDinnerPCResult4}}"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>
                            <script src="/weekender_assets/js/modal_lightbox_fifth.min.js"></script>
                            </body>
                        </div>
                    @endif
                    @if($data->augCorePC)
                        <div class="col-md-3">
                            <h4 style="color: red;text-align: center;">August Core Post Card</h4>
                            <body>
                            <div class="modal-wrap">
                                <div class="slider-big">
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCorePCResult1}}"
                                       data-lightbox-second="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCorePCResult1}}"
                                            style="width: 20%;margin: 0 auto;" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCorePCResult2}}"
                                       data-lightbox-second="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augCorePCResult2}}"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>
                            <script src="/weekender_assets/js/modal_lightbox_second.min.js"></script>
                            </body>
                        </div>
                    @endif
                    @if($data->augWeekendReminderPC2)
                        <div class="col-md-3">
                            <h4 style="color: red;text-align: center;">August Weekend Reminder 08/22 Post Card</h4>
                            <body>
                            <div class="modal-wrap">
                                <div class="slider-big">
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC2Result1}}"
                                       data-lightbox="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/lo_res/{{$data->augWeekendReminderPC2Result1}}"
                                            style="width: 20%;margin: 0 auto;" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC2Result2}}"
                                       data-lightbox="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/lo_res/{{$data->augWeekendReminderPC2Result2}}"
                                            style="width: 100%" alt="">
                                    </a>
                                </div>
                            </div>
                            <script src="/weekender_assets/js/modal_lightbox.min.js"></script>
                            </body>
                        </div>
                    @endif
                    @if($data->augWeekendReminderPC3)
                        <div class="col-md-3">
                            <h4 style="color: red;text-align: center;">August 08/29 Weekend Reminder Post
                                Card</h4>
                            <body>
                            <div class="modal-wrap">
                                <div class="slider-big">
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC3Result1}}"
                                       data-lightbox-second="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC3Result1}}"
                                            style="width: 20%;margin: 0 auto;" alt="">
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC3Result2}}"
                                       data-lightbox-second="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/{{$data->augWeekendReminderPC3Result2}}"
                                            style="width: 100%" alt="">
                                    </a>

                                </div>
                            </div>
                            <script src="/weekender_assets/js/modal_lightbox_second.min.js"></script>
                            </body>
                        </div>
                    @endif
                    @if($data->augCruise2PC)
                        <body>
                        <div class="col-md-3">

                            <div class="modal-wrap">
                                <h4 style="color: red;text-align: center;">September Cruise 2 Post Card</h4>

                                <div class="slider-big">
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/130725_AUG2021_CRUISE_02_PC_P01.jpg"
                                       data-lightbox-second="roadtrip">
                                        <center><img
                                                src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/130725_AUG2021_CRUISE_02_PC_P01.jpg"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/130725_AUG2021_CRUISE_02_PC_P02.jpg"
                                       data-lightbox-second="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/august_2021/hi_res/130725_AUG2021_CRUISE_02_PC_P02.jpg"
                                            alt="">
                                    </a>

                                </div>
                            </div>
                        </div>
                        <script src="/weekender_assets/js/modal_lightbox_second.min.js"></script>

                        </body>
                    @endif
                    @if($data->sepReminderPC)
                        <body>
                        <div class="col-md-3">
                            <div class="modal-wrap">
                                <h4 style="color: red;text-align: center;">350k Reminder Post Card</h4>

                                <div class="slider-big">
                                    <a href="/PostCardImage/350ReminderPostCard/ImageByAccountId/{{$data->BAC_Account_Combined}}.jpg"
                                       data-lightbox-text="roadtrip">
                                        <center><img
                                                src="/PostCardImage/350ReminderPostCard/ImageByAccountId/{{$data->BAC_Account_Combined}}.jpg"
                                                style="width: 20%" alt=""></center>
                                    </a>
                                    <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepReminderPCResult2}}"
                                       data-lightbox-text="roadtrip">
                                        <img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepReminderPCResult2}}"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <script src="/weekender_assets/js/modal_lightbox_text.min.js"></script>

                        </body>
                    @endif
                    @if($data->sepCorePC)
                        <body>
                        <div class="modal-wrap">
                            <h4 style="color: red;text-align: center;">September Core Post Card</h4>

                            <div class="slider-big">
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult1}}"
                                   data-lightbox-fourth="roadtrip">
                                    <center><img
                                            src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult1}}"
                                            style="width: 75%" alt=""></center>
                                </a>
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult2}}"
                                   data-lightbox-fourth="roadtrip">
                                    <img
                                        src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult2}}"
                                        alt="">
                                </a>
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult3}}"
                                   data-lightbox-fourth="roadtrip">
                                    <img
                                        src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult3}}"
                                        alt="">
                                </a>
                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult4}}"
                                   data-lightbox-fourth="roadtrip">
                                    <img
                                        src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/september_2021/hi_res/{{$data->sepCorePCResult4}}"
                                        alt="">
                                </a>
                            </div>
                        </div>

                        <script src="/weekender_assets/js/modal_lightbox_fourth.min.js"></script>
                        <script src="/weekender_assets/js/modal_slick.min.js"></script>
                        <script src="/weekender_assets/js/modal_main.js"></script>
                        </body>
                    @endif
                </div>
                <script src="/weekender_assets/js/modal_slick.min.js"></script>
                <script src="/weekender_assets/js/modal_main.js"></script>
            @else
                We do not have anything for you to see at this time, please check back later.
            @endif
            <br>
        </div>
        <div class="col-md-3">
            <a href="/" rel="nofollow">
                @switch($data->BAC_Tier)
                    @case('Red')
                    <img style="width:100%" src="{{ asset('assets/images/cards/red.png') }}" alt="bally's card"></a>
            <h4 style="font-weight: 600;color: #232325;">
                Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
            <h5 style="color:red;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
            <div id="TierLevel" class="textlineRed">Tier Level Red</div>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Score <span
                        style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Points <span
                        style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                    <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                            class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                </div>
            </div>
            <br>
            <hr style="height:1px;border:none;color:red;background-color:red;"/>
            @break
            @case('Black')
            <img style="width:100%" src="{{ asset('assets/images/cards/black.png') }}" alt="bally's card"></a>
            <h4 style="font-weight: 600;color: #232325;">
                Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
            <h5 style="color:red;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
            <div id="TierLevel" class="textlineBlack">Tier Level Black</div>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Score <span
                        style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Points <span
                        style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                    <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                            class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                </div>
            </div>
            <br>
            <hr style="height:1px;border:none;color:red;background-color:black;"/>
            @break
            @case('Platinum')
            <img style="width:100%" src="{{ asset('assets/images/cards/platinum.png') }}" alt="bally's card"></a>
            <h4 style="font-weight: 600;color: #232325;">
                Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
            <h5 style="color:red;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
            <div id="TierLevel" class="textlinePlatinum">Tier Level Platinum</div>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Score <span
                        style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Points <span
                        style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                    <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                            class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                </div>
            </div>
            <br>
            <hr style="height:1px;border:none;color:red;background-color:#e5e4e2;"/>
            @break
            @default
            <img style="width:100%" src="{{ asset('assets/images/cards/gold.png') }}" alt="bally's card"></a>
            <h4 style="font-weight: 600;color: #232325;">
                Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
            <h5 style="color:red;text-align: center;"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                    href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
            <div id="TierLevel" class="textlineGold">Tier Level Gold</div>
            <div id="yourMembershipLevel">
                <div id="pointsBalanceNextLevel">Tier Score <span
                        style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                </div>
                <div id="pointsBalanceNextLevel"> Reward Points <span
                        style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                    <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                            class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                </div>
            </div>
            <br>
            <hr style="height:1px;border:none;color:red;background-color:#FFD700;"/>
            @endswitch
        </div>
    </div>
@endsection


